from docx import Document

import tempfile
import win32api
import win32print

filename = tempfile.mktemp('print.docx')
document = Document()
document.add_heading('PT. PANCA BUDI LOGISTINDO2', level=2)
document.add_heading()
document.save(filename) # Save document

open(filename, "r")
win32api.ShellExecute(
  0,
  "print",
  filename,
  #
  # If this is None, the default printer will
  # be used anyway.
  #
  '/d:"%s"' % win32print.GetDefaultPrinter(),
  ".",
  0)
