employee_peer_feedback_id.action_submit()
if employee_peer_feedback_id and employee.user_id.partner_id.id: 
    message = _("This Employee Performance Record has been created: <a href=# data-oe-model=employee.peer.feedback data-oe-id=%d>%s</a>") % (employee_peer_feedback_id.id, employee.name)
    try:
        employee_peer_feedback_id.message_post(body=message, subtype='mt_comment', partner_ids=[employee.user_id.partner_id.id])
    except Exception:
        pass
