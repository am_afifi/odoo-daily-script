@api.model
    def create(self, vals):
        values = {}
        mail_mail = self.env['mail.mail']
        
        # context: no_log, because subtype already handle this
        result = super(HelpdeskTicket, self.with_context(mail_create_nolog=True)).create(vals)
 
        values['subject'] = 'ePillars Support Case Created - %s  [%s]' % (result.name,result.id)
        values['body_html'] = """<html>
                                <head></head>
                                <body>    
                                Case Number:    """+str(result.id) +"""<br/>
                                Support Account Name:   """+ str(result.partner_id.name)+"""<br/>
                                Support Account Email:  """+ str(result.partner_email)+"""<br/>
                                Status: New <br/>
                                Subject:    """+str(result.name) +"""<br/><br/><br/>
 
 
                                You will not be able to 'Reply' to this e-mail. If you wish to contact us,<br/> 
                                Please call +97143263939.
                                </body>
                                </html>
                                """
        values['email_from'] = 'ePillars Support<support@epillars.com>'
        values['email_to'] = result.partner_email
        values['reply_to'] = 'noreply<noreply@epillars.com>'
        if values['email_to']:
            mail= mail_mail.create(values)
        return result
