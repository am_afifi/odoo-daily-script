@api.model
def _create_sequence(self, vals, refund=False):
    """ Create new no_gap entry sequence for every new Journal"""
    prefix = self._get_sequence_prefix(vals['code'], refund)
    seq = {
        'name': refund and vals['name'] + _(': Refund') or vals['name'],
        'implementation': 'no_gap',
        'prefix': prefix,
        'padding': 4,
        'number_increment': 1,
        'use_date_range': True,
        }
    if 'company_id' in vals:
        seq['company_id'] = vals['company_id']
    return self.env['ir.sequence'].create(seq)
