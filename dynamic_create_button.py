from xml.etree import ElementTree as etree

    @api.model
    def fields_view_get(self, view_id=None, view_type='form',
                        toolbar=False, submenu=False):
        result = super(IrAttachmentInherit, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)

        # membuat dynamic visible button create pada tree view
        is_admin_legal = self.env.user.has_group('document_directory_extension.group_document_manager')
        is_user_legal = self.env.user.has_group('custom_security_superindo.group_user_legal')
        is_user_non_legal = self.env.user.has_group('custom_security_superindo.group_user_non_legal')
        
        if view_type != 'search':
            if self.env.user.has_group('custom_security_superindo.group_user_readonly') \
                and not (is_admin_legal or is_user_legal or is_user_non_legal):
                xml_view = etree.fromstring(result['arch'])
                xml_view.set('create', 'false')
                xml_view.set('edit', 'false')
                xml_view.set('delete', 'false')
                result['arch'] = etree.tostring(xml_view)
                
        return result
