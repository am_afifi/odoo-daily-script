    @api.multi
    def action_approve(self):        
        for record in self:
            pick_lines = []
            for line in record.request_line_ids:
                pick_line_values= {
                    'name': line.product_id.name,
                    'product_id': line.product_id.id,
                    'product_uom_qty': line.issued_product_qty,
                    'product_uom': line.product_id.uom_id.id,
                    'state': 'draft',
                }
                pick_lines.append((0,0,pick_line_values))
            picking = {                
                'location_id': 15,
                'location_dest_id': record.location_dest_id.id,
                'move_type':'direct',            
                'picking_type_id': 5,
                'ctsrf':record.id,
                'move_lines':pick_lines,
            }
            transfer = self.env['stock.picking'].sudo().create(picking)
            if transfer:
                record.state = 'approved'
                record.approved_date = fields.Datetime.now()
                record.approved_by = self.env.uid
            else:
                raise ValidationError(_("Something went wrong during your Request generation"))
        return True
