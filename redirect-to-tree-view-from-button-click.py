#.py file

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo import tools


class Project(models.Model):
    _inherit = "project.project"

    @api.multi
    def action_view_project_cashflow_report(self):
        project_ids = self.id
        return{
            'name'          :   ('Project Cashflow Report'),
            'type'          :   'ir.actions.act_window',
            'view_type'     :   'form',
            'view_mode'     :   'pivot',
            'target'        :   'new', 
            'res_model'     :   'project.cashflow.report',
            'view_id'       :   False,
            'domain'        :   [('project_id','in',[project_ids])]
            }
      }


#.xml file

<xpath expr="//button[@name='attachment_tree_view']" position="after">
<button class="oe_stat_button" name="action_view_project_cashflow_report"
        string="Cash Flow" type="object"
        icon="fa-bars"/>
</xpath>
