odoo.define('o_menu_brand_replace', function (require) {
    "use strict";

    var Menu = require('web.Menu');
    // var Menu = require('web_enterprise.Menu');
    var core = require('web.core');
    var QWeb = core.qweb;
    var session = require('web.session');

    Menu.include({
        _updateMenuBrand: function (brandName) {
            this._super.apply(this, arguments);             
            if (brandName === "Sale") {
                this.$menu_brand_placeholder.html("<img src='/web/image?model=res.company&id=1&field=logo'style='width: 95px;height: 45px;'/>");                
            }
        }
    })

    return {
        'Menu': Menu,
    };
});

#XML
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <template id="assets_backend" name="esign assets" inherit_id="web.assets_backend">
        <xpath expr="." position="inside">
            <script type="text/javascript" src="/esign/static/src/js/o_menu_brand_replace.js"></script>
        </xpath>
    </template> 
</odoo>
