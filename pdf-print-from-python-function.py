class AMCPaintBooth(models.Model):
    _name = 'amc.paint.booth'   
    @api.multi
    def action_amc_jobcard_pb_print(self):
        context = {
                'lang': 'en_US', 
                'active_ids': [self.id], 
                'tz': False, 
                'uid': 1
                }
        return {
            'context': context,
            'data': None,
            'type': 'ir.actions.report.xml',
            'report_name': 'safe_amc.safe_report_amc_jobcard_pb',
            'report_type': 'qweb-pdf',
            'report_file': 'safe_amc.safe_report_amc_jobcard_pb',
            'name': 'AMC',
        }
