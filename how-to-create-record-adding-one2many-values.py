class sale_order(models.Model):
    _inherit = "sale.order"
    
    #By default load the terms and conditions configured in the master T&C configuration
    @api.model
    def default_get(self,vals):
        terms = []
        terms_obj = self.env['so.terms.config']
        termsids = terms_obj.search([])
        for rec in termsids:
            terms.append((0, 0, {'name':rec.name,'description' : rec.description}))
        res = super(sale_order, self).default_get(vals)      
        res.update({'terms': terms})
        return res

    terms = fields.One2many('sale.order.terms.conditions','order_id',string='Terms &amp; Conditions',copy=True)
