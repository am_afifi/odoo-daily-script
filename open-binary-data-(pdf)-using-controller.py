import base64
import logging
import werkzeug
 
from odoo import http, _
from odoo.exceptions import AccessError, UserError
from odoo.http import request
 
 
#URL :- http://192.168.56.11:8069/web/binary/download_pdf/12
class DownloadPDF(http.Controller):
    @http.route('/web/binary/download_pdf/<id>', type='http', auth="public",website=True)
    def download_pdf(self,id,**kw):        
        response = werkzeug.wrappers.Response()
        slide_slide_obj = request.env['slide.slide'].sudo().search([('id','=',id)])
        response.data = slide_slide_obj.datas and slide_slide_obj.datas.decode('base64') or ''
        response.mimetype = 'application/pdf'
        return response
