mkdir "/opt/odoo/backup/"
chown -R postgres:postgres /opt/odoo/backup
sudo su postgres
pg_dump -E UTF-8 -p 5432 -F p -b -f /opt/odoo/backup/<backupfile> <databasename>
sudo su postgres
createdb --encoding=UTF-8 live_db
psql meet_crm < live_db.sql
