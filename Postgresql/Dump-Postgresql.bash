OPTION 1
Dump DB
$ sudo pg_dump --no-owner db_name > /opt/db_name.sql or .dump
 
Restore DB
$ sudo su postgres
$ psql
$ create database db_name;
$ psql db_name > db_name.sql
OPTION 2
Dump DB
$ sudo pg_dump --no-owner db_name | gzip > /opt/db_name.sql.gz
 
Restore DB
createdb  db_name -T template1 -U odoo -h localhost
zless db_name.sql.gz | psql db_name
