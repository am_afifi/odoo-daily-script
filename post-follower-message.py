body = "Follower Message!"
 
records = self._get_followers(cr, uid, ids, None, None, context=context)
followers = records[ids[0]]['message_follower_ids']
self.message_post(cr, uid, ids, body=body, subtype='mt_comment', partner_ids=followers, context=context)
