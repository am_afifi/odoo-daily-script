from odoo import models,fields,api,_,SUPERUSER_ID
 
class SaleOrder(models.Models):
    _name = "sale.order"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submitted', 'Submitted'),
        ('paid', 'Paid'),
        ],default='draft',string='Status', copy=False, index=True,         readonly=True, store=True,track_visibility='always',
 
    @api.multi
    def action_submit(self):        
        self.state = 'submitted'
        body =_("Expense Advance Submitted")
        subject = _("Expense Advance")
        self.sudo().message_post(body=body,subject=subject, message_type="notification", subtype="mail.mt_comment", author_id=self.partner.id)
        #you can use author_id=SUPERUSER_ID

#.xml file after sheet you can place this.
<div class="oe_chatter">
   <field name="message_follower_ids" widget="mail_followers" groups="base.group_user"/>
   <field name="message_ids" widget="mail_thread"/>
</div>
