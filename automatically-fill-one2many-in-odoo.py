#T&C in each SO
class sale_order(models.Model):
    _inherit = "sale.order"
     
    #By default load the terms and conditions configured in the master T&C configuration
    @api.model
    def _default_so_tc(self):
        terms_obj = self.env['so.terms.config']
        terms = []
        termsids = terms_obj.search([])
        for rec in termsids:
            values = {}
            values['name'] = rec.name
            values['description'] = rec.description
            terms.append((0, 0, values))
        return terms
 
    terms = fields.One2many('sale.order.terms.conditions','order_id',string='Terms and Conditions',copy=True,default=_default_so_tc)
