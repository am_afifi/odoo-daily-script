options="{'no_quick_create':True,'no_create_edit':True,'no_open': True,}"

OR

options="{'no_create':True,'no_open': True}"

<field name="Your_many2one_field" widget="selection">

For your ref:

no_quick_create - It will remove Create and edit... option.
no_create_edit - It will remove Create "entered text" option.
no_create - no_quick_create and no_create_edit combined.
no_open - in read mode: do not render as a link.



# INVENTORY VALUATION PROCEDURE #

1. Create Drum UOM
2. Replace old transaction purchase_order_line.product_uom = '85' with 87
3. Replace stock_move_line.product_uom_id = '85' with 87
4. Replace stock_move.product_uom = '85' with 87

# DAILY PROGRAMMING
1. Related Field Example
product_type = fields.Selection(related='product_id.product_tmpl_id.type')

$ to make related field, make sure both field type at the same type or will show error.
$ related field need field (contain ID) that related an another like many2one.

# ID PRODUCT.PRODUCT PRODUCT.TEMPLATE PURCHASE.ORDER PURCHASE.ORDER.LINE

product.product = 8352
product_tmpl_id = 14280

# EXAMPLE

# Compute of unaffected earnings account
    @api.depends('company_id')
    def _compute_unaffected_earnings_account(self):
        account_type = self.env.ref('account.data_unaffected_earnings')
        self.unaffected_earnings_account = self.env['account.account'].search(
            [
                ('user_type_id', '=', account_type.id),
                ('company_id', '=', self.company_id.id)
            ])

    unaffected_earnings_account = fields.Many2one(
        comodel_name='account.account',
        compute='_compute_unaffected_earnings_account',
        store=True
    )

# PRIVATE METHOD START WITH UNDERSCORE (_)


# MENU OVERRIDE
you can use the following example

<record id="original_module.menu_id" model="ir.ui.menu">
    <field name="groups_id" eval="[(3,ref('my_new_group_id'))]"/>
</record>
There are actually0-6 numbers for representing each job for a many2many/ one2many field

(0, 0, { values }) -- link to a new record that needs to be created with the given values dictionary
(1, ID, { values }) -- update the linked record with id = ID (write values on it)
(2, ID) -- remove and delete the linked record with id = ID (calls unlink on ID, that will delete the object completely, and the link to it as well)
(3, ID) -- cut the link to the linked record with id = ID (delete the relationship between the two objects but does not delete the target object itself)
(4, ID) -- link to existing record with id = ID (adds a relationship)
(5) -- unlink all (like using (3,ID) for all linked records)
(6, 0, [IDs]) -- replace the list of linked IDs (like using (5) then (4,ID) for each ID in the list of IDs)

# MAIL (CHATTER ODOO)
-field attribut track_visibility='onchange' / track_visibility='always', digunakan untuk membuat log pada mail thread perubahan status


# INSERT LINES FROM OTHER DOCUMENT
invoice_id = fields.Many2one('account.invoice', string='Vendor Bill')
    
    @api.onchange('invoice_id')
    def _onchange_invoice_id(self):
        self.cost_lines = None
        if self.invoice_id:
            invoice_lines = []
            for line in self.invoice_id.invoice_line_ids:
                invoice_lines.append(
                    (0, 0,
                     {
                         'product_id': line.product_id.id,
                         'name': line.product_id.name or '',
                         'account_id':  line.product_id.property_account_expense_id.id or line.product_id.categ_id.property_account_expense_categ_id.id,
                         'split_method': line.product_id.split_method,
                         'price_unit': line.price_subtotal,
                     }
                     ))
            self.cost_lines = invoice_lines

# Untuk membuat field readonly namun bisa save
readonly="1" force_save="1"

# ACCESS RIGHT
id,name,model_id:id,group_id:id,perm_read,perm_write,perm_create,perm_unlink
id              access_account_invoice,
name            account.invoice.purchase_user,
model_id:id     account.model_account_invoice,
group_id:id     purchase.group_purchase_user,
1,1,1,0

# RECORD RULE
	<record model="res.groups" id="purchase_manager_pusat">
            <field name="name">Purchase / Manager Pusat</field>
            <field name="users" eval="[(4, ref('base.user_root'))]"/>
        </record>

        <record model="ir.rule" id="record_rule_purchase_user">
            <field name="name">Record Rule Purchase User</field>
            <field name="model_id" ref="purchase_blanket_order.model_purchase_blanket_order"/>
            <field name="domain_force">['|', ('branch_id', 'in', [b.id for b in user.branch_ids]), ('branch_id', 'in',
                [b.id for b in user.branch_id])]
            </field>
            <field name="groups" eval="[(4, ref('purchase.group_purchase_user'))]"/> 
		MULTI GROUPS
	    <field name="groups" eval="[(4, ref('sales_team.group_sale_manager')), (4, ref('sales_team.group_sale_salesman'))]"/>
        </record>


# CONSTRAINTS
@api.constrains('field_name')
    def check_field(self):
        if not self.field_name:
            raise ValidationError(_('Enter the Value'))

# DOMAIN & GET DEFAULT
@api.model
    def _get_default(self):
        branch_obj = self.env['res.users'].search([('id', '=', self.env.user.id)]).branch_id.id
        return branch_obj

    @api.model
    def _get_selected_branch_id(self):
        return ['|', ('id', 'in', [b.id for b in self.env.user.branch_ids]),
                ('id', 'in', [b.id for b in self.env.user.branch_id])]

# DYNAMIC DOMAIN 1
    @api.onchange('jenis_blanket')
    def _onchange_jenis_blanket(self):
        res = {}
        if self.jenis_blanket == 'penjualan_jarak_jauh':
            res.update({
                'domain': {
                    'branch_id': []
                }
            })
            return res
        else:
            branch_id_obj = self.env['res.users'].search([('id','=',self.env.user.id)])
            domain = []
            
            for r in branch_id_obj:
                if r.branch_id.id:
                    domain.append(r.branch_id.id)
                if r.branch_ids.ids:
                    domain += r.branch_ids.ids

            res.update({
                'domain': {
                    'branch_id': [('id','in',domain)]
                }
            })
            return res

# DYNAMIC DOMAIN 2
    @api.onchange('state')
    def onchange_method_state(self):
        return {
            'value': {'state_id': False},
            'domain': {'state_id': [('country_id.code', '=', 'ID')]}
        }

# MESSAGE POST
    def _update_line_quantity(self, values):
        orders = self.mapped('order_id')
        for order in orders:
            order_lines = self.filtered(lambda x: x.order_id == order)
            msg = "<b>The ordered quantity has been updated.</b><ul>"
            for line in order_lines:
                msg += "<li> %s:" % (line.product_id.display_name,)
                msg += "<br/>" + _("Ordered Quantity") + ": %s -> %s <br/>" % (
                line.product_uom_qty, float(values['product_uom_qty']),)
                if line.product_id.type in ('consu', 'product'):
                    msg += _("Delivered Quantity") + ": %s <br/>" % (line.qty_delivered,)
                msg += _("Invoiced Quantity") + ": %s <br/>" % (line.qty_invoiced,)
            msg += "</ul>"
            order.message_post(body=msg)
