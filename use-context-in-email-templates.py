#in module.py file

local_context = self.env.context.copy()
local_context.update({
                    'email_to': 'ananthu@epillars.com',
                    'email_from': 'admin@epillars.com',
                    'name': 'Passport',
                    'document_number': 'H89746023',
                    'expiry_date': '12-12-2017',
                    'description': 'Please Check',
                    })
template = self.env.ref('hr_dcoument_manager.email_template_employee_document_reminder_mail',False)
template.with_context(local_context).send_mail(self.id, raise_exception=True)

#In email template code you can access context as:
<div>Document Name: <strong>${ctx['name']}</strong><br/></div>
<div>Document ID: <strong>${ctx['document_number']}</strong></div>
