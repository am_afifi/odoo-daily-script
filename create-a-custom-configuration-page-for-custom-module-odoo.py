#RES_CONFIG_SETTINGS.PY
from odoo import fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    signature_width = fields.Char("width",default='13')
    signature_height = fields.Char("height")
    stamp_width = fields.Char("width")
    stamp_height = fields.Char("height")

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param        
        res.update(
            signature_width = get_param('esign.signature_width') if get_param('esign.signature_width') else '120',
            signature_height = get_param('esign.signature_height') if get_param('esign.signature_height') else '53',
            stamp_width = get_param('esign.stamp_width') if get_param('esign.signature_width') else '120',
            stamp_height = get_param('esign.stamp_height') if get_param('esign.signature_height') else '53',
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].sudo().set_param
        set_param('esign.signature_width', self.signature_width)
        set_param('esign.signature_height', self.signature_height)
        set_param('esign.stamp_width', self.stamp_width)
        set_param('esign.stamp_height', self.stamp_height)

#RES_CONFIG_SETTINGS_VIEWS.XML
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <data>

    <record id="res_config_settings_view_form" model="ir.ui.view">
        <field name="name">res.config.settings.view.form.inherit.esign</field>
        <field name="model">res.config.settings</field>
        <field name="priority" eval="65"/>
        <field name="inherit_id" ref="base.res_config_settings_view_form"/>
        <field name="arch" type="xml">
            <xpath expr="//div[hasclass('settings')]" position="inside">                
                <div class="app_settings_block" data-string="Events" string="eSign" data-key="esign">
                    <h2>Signature Type Width and Height</h2>
                    <div class="row mt16 o_settings_container" id="signatute_type_settings">                        
                        <div class="col-6 col-lg-6 o_setting_box" id="signatute_size_setting">
                            <div class="o_setting_right_pane">
                                <span class="o_form_label">Signature</span>                                
                                <div class="text-muted">
                                    Set Signature Coordinate Width and Height here.
                                </div>
                                <div class="content-group">
                                    <div class="row mt16">
                                        <div class="col-6">
                                            <label class="o_light_label" string="Width" for="signature_width"/>
                                            <field name="signature_width"/><span> px</span>
                                        </div>
                                        <div class="col-6">
                                            <label class="o_light_label" string="Height" for="signature_height"/>
                                            <field name="signature_height"/><span> px</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-lg-6 o_setting_box" id="signatute_size_setting">
                            <div class="o_setting_right_pane">
                                <span class="o_form_label">Stamp</span>                                
                                <div class="text-muted">
                                    Set Stamp Coordinate Width and Height here.
                                </div>
                                <div class="content-group">
                                    <div class="row mt16">
                                        <div class="col-6">
                                            <label class="o_light_label" string="Width" for="stamp_width"/>
                                            <field name="stamp_width"/><span> px</span>
                                        </div>
                                        <div class="col-6">
                                            <label class="o_light_label" string="Height" for="stamp_height"/>
                                            <field name="stamp_height"/><span> px</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </xpath>
        </field>
    </record>
    
    <record id="action_esign_configuration" model="ir.actions.act_window">
        <field name="name">Settings</field>
        <field name="type">ir.actions.act_window</field>
        <field name="res_model">res.config.settings</field>
        <field name="view_mode">form</field>
        <field name="target">inline</field>
        <field name="context">{'module' : 'esign'}</field>
    </record>

    <menuitem id="menu_esign_config" name="Configuration" parent="esign.menu_esign_root"
            sequence="100" groups="base.group_no_one"/>

    <menuitem id="menu_esign_global_settings" name="Settings"
            parent="esign.menu_esign_config" sequence="0" action="esign.action_esign_configuration"/>
    </data>
</odoo>
