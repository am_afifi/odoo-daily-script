class MyModel(models.Model):
    _name = 'my.model'
 
    image = fields.Binary('Image', required=True)
    image_filename = fields.Char("Image Filename")

# xml
<record id="view_form_my_model" model="ir.ui.view">
  <field name="name">My Model Form</field>
  <field name="model">my.model</field>
  <field name="arch" type="xml">
    <form>
      <sheet>
        <group name="group_top">
          <group name="group_left">
            <field name="image_filename" invisible="0"/>
            <field widget="binary" height="64" name="image" filename="image_filename" />
          </group>
          <group name="group_right">
          </group>
        </group>
      </sheet>
    </form>
  </field>
</record>
