#Create a custom model

class ProjectMilestoneInvoicing(models.Model):
    _name = "project.milestone.invoicing"
    _auto = False

    project = fields.Many2one('project.project')
    client = fields.Many2one('res.partner')
    milestone = fields.Char(string='Milestone')
    payment = fields.Float(string='Payment')

    @api.model_cr
    def init(self):
        print "Connected"
        tools.drop_view_if_exists(self._cr, 'project_milestone_invoicing')
        self._cr.execute("""
            CREATE OR REPLACE VIEW project_milestone_invoicing AS (
                SELECT 
                row_number() OVER () as id,
                ps.project_id as project,
                ps.milestone as milestone,
                ps.payment as payment, 
                aa.partner_id as client 
                FROM public.project_stages ps 
                LEFT JOIN  project_project pp ON ps.project_id = pp.id
                LEFT JOIN  account_analytic_account aa ON pp.analytic_account_id = aa.id
                where  ps.milestone is not null and ps.project_id is not null
            )""")

#Create a View

<!-- Form view Action for Project Milestone Invoicing -->
        <record id="action_project_milestone_invoicing_view" model="ir.actions.act_window">
            <field name="type">ir.actions.act_window</field>
            <field name="name">Project Milestone Invoicing</field>
            <field name="res_model">project.milestone.invoicing</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="help" type="html">
                <p class="oe_view_nocontent_create">Create the first milestone</p>
            </field>
        </record>

        <record model="ir.ui.view" id="action_project_milestone_invoicing_tree">
            <field name="name">Project Milestone Invoicing</field>
            <field name="model">project.milestone.invoicing</field>
            <field name="arch" type="xml">
                <tree string="Project Milestone Invoicing" create="false">
                    <field name='project'/>
                    <field name='client' />
                    <field name='milestone' />
                    <field name='payment' />
                </tree>
            </field>
        </record>


#Create Menu

<!-- Menu item for Project Milestone Invoicing-->
        <menuitem name="Project Milestone Invoicing" action="action_project_milestone_invoicing_view" id="menu_project_milestone_invoicing_act" parent="project.menu_project_config" sequence="4"/>
