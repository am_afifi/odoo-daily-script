#__manifest__.py
{
    .......
    "depends": [
        "web",
        "hr",
    ],
    "data": [
        "views/assets.xml",        
    ],
    "qweb": ["static/src/xml/qweb.xml"],
}


#assets.xml
<?xml version="1.0" encoding="UTF-8"?>
<odoo>
    <template id="assets_backend_inherit_ninebox" inherit_id="web.assets_backend" name="Employees Asset">        
        <xpath expr="script[last()]" position="after">            
            <script type="text/javascript" src="/leaves_summary_create_leave_button/static/src/js/listview_button.js"></script>
            <link href="/leaves_summary_create_leave_button/static/src/css/style.css" rel="stylesheet" type="text/css"/>           
        </xpath>
    </template>
</odoo>


#qweb.xml
<?xml version="1.0" encoding="UTF-8"?>
<templates id="template" xml:space="preserve">

    <t t-extend="ListView.buttons">
        <t t-jquery="div.o_list_buttons" t-operation="append">            
            <t t-if = "widget.modelName == 'hr.leave.report'">               
                <button type="button" class="btn btn-primary o_crete_leave_from_summary">
                    Apply Leave
                </button>      
            </t>      
        </t>
    </t>

</templates>


#listview_button.js
odoo.define('leaves_summary_create_leave_button.listview_button', function (require) {
    "use strict";

    var core = require('web.core');
    var ListView = require('web.ListView');
    var ListController = require("web.ListController");

    var IncludeListView = {

        renderButtons: function() {
            this._super.apply(this, arguments);
            if (this.modelName === "hr.leave.report") {
                var summary_apply_leave_btn = this.$buttons.find('button.o_crete_leave_from_summary');              
                summary_apply_leave_btn.on('click', this.proxy('crete_leave_from_summary'))
            }
        },
        crete_leave_from_summary: function(){
            var self = this;
            var action = {
                type: "ir.actions.act_window",
                name: "Leave",
                res_model: "hr.leave",
                views: [[false,'form']],
                target: 'new',
                views: [[false, 'form']], 
                view_type : 'form',
                view_mode : 'form',
                flags: {'form': {'action_buttons': true, 'options': {'mode': 'edit'}}}
            };
            return this.do_action(action);
        },

    };
    ListController.include(IncludeListView);
});
