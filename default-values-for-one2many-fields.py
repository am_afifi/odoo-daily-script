@api.model
    def default_get(self, fields):
        vals = super(LibraryManagement, self).default_get(fields)
        vals['note'] = "Test Data"
        library_management_lines = [(5,0,0)]
        books_rec = self.env['library.books'].search([])
        for rec in books_rec:
            line = (0,0,{
                'library_management_id' : self.id,
                'book_id': rec.id,
                'issue_date': date.today(),
            })
            library_management_lines.append(line)
        vals['library_management_line_ids'] = library_management_lines
        return vals
