# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def create_action(self):
        order_line = [
                (0, 0, {
                        'product_id': 23,
                        'product_uom_qty': 1.00,
                }),
                (0, 0, {
                        'product_id': 15,
                        'product_uom_qty': 2.00,
                })
            ]

        sale_order = self.env['sale.order'].create({
            'partner_id': 14,
            'partner_invoice_id': 14,
            'partner_shipping_id': 14,
            'order_line': order_line,
            'pricelist_id': 1,
            'picking_policy': 'direct',
        })
        so = self.env['sale.order'].create({
            'partner_id': self.partner_customer_usd.id,
            'partner_invoice_id': self.partner_customer_usd.id,
            'partner_shipping_id': self.partner_customer_usd.id,
            'order_line': [(0, 0, {
                'name': prod_gap.name, 
                'product_id': prod_gap.id, 
                'product_uom_qty': 2, 
                'product_uom': prod_gap.uom_id.id,
                'price_unit': prod_gap.list_price})],
            'pricelist_id': self.pricelist_usd.id,
        })
        so.action_confirm()
        so._create_analytic_account()
