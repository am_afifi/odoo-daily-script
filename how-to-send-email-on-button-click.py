#Create mail template
<?xml version="1.0" ?>
<odoo>
    <data noupdate="0">
        <!--Email template -->
        <record id="email_template_employee_peer_feedback" model="mail.template">
            <field name="name">Employee Peer Feedback</field>
            <field name="model_id" ref="employee_peer_feedback.model_employee_peer_feedback"/>
            <field name="subject">Employee Peer Feedback</field>
            <field name="email_from">${('&lt;%s&gt;' % (object.env.user.company_id.email or user.email)) | safe}</field>
            <field name="recipient_ids">${object.recipient_ids|safe}</field>
            <field name="use_default_to" eval="True"/>
            <field name="body_html" type="html">
                <div>${object.body_html}</div>                
            </field>
            <field name="auto_delete" eval="True"/>
            <field name="user_signature" eval="True"/>
        </record>

    </data>
</odoo>

#Create button
<button name="action_submit" type="object"
                            string="Submit 2"                            
                            groups="hr.group_hr_manager,hr.group_hr_user"/>

#Python function
@api.multi
    def action_submit(self):
        self.ensure_one()
        
        template_obj = self.env['mail.template'].sudo().search([('name','=','Employee Peer Feedback')], limit=1)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        
        for peers in self.peer_employee_ids:
            _url = ''+ base_url +'/peer_feedback/'+ str(self.id) +'/'
            if peers.user_id.partner_id.id:
                body_html = '<p>Dear '+ peers.name +',<br />\
                    We request your feedback for the employee '+ self.employee_id.name +' as part \
                    of the Performance Review. Click on the link below to submit your \
                    comments.</p><br /><p><a href=%s class="btn btn-danger">Employee Peer Feedback</a><br></p>' % _url

                if template_obj:
                    mail_values = {                        
                        'body_html': body_html,
                        'recipient_ids': [(4, peers.user_id.partner_id.id)]
                    }
                    create_and_send_email = self.env['mail.mail'].create(mail_values).send() 
