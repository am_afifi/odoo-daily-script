# required https://github.com/OCA/reporting-engine/tree/14.0/report_xlsx

<a role="button" t-if="orders"
   t-att-href="'/my/export_orders?order_ids=%s' % orders.ids"
   class="btn btn-primary mb-2 mt-2"
   target="_blank"
>

class CustomerPortalExtended(CustomerPortal):
    @http.route('/my/export_orders', type='http', auth="user", website=True)
    def export_orders(self, **kw):
        order_ids = ast.literal_eval(kw['order_ids'])
        report_sudo = request.env.ref(report_name).with_user(SUPERUSER_ID)
        report = report_sudo._render_xlsx(order_ids, {})[0]
        report_http_headers = [
            ('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
            ('Content-Length', len(report)),
            ('Content-Disposition', content_disposition("Orders.xlsx"))
        ]
        return request.make_response(report, headers=report_http_headers)
