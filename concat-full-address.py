@api.onchange('street_ht', 'street2_ht', 'city_ht', 'zip_ht', 'state_id_ht')
    def onchange_method_applicant_address(self):
        for x in self:
            address = ''
            if x.street_ht:
                address = x.street_ht
            if x.street2_ht:
                address = '%s, %s' % (address, x.street2_ht)
            if x.city_ht:
                address = '%s, %s' % (address, x.city_ht)
            if x.zip_ht:
                address = '%s, %s' % (address, x.zip_ht)
            if x.state_id_ht:
                address = '%s, %s' % (address, x.state_id_ht.name)

            x.full_address = address
