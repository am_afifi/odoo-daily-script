<notebook colspan="4">
    <page string="Cash Inflow Planning">
        <field name="cash_inflow_ids" context="{'project_id': parent.id}">
            <tree editable="bottom">
                <field name="period_date"/>
                <field name="deliverables_ids" widget="many2many_checkboxes" domain="[('project_id','=',context.get('project_id'))]"/>
                <field name="name"/>
            </tree>
        </field>
    </page>
</notebook>
