# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from itertools import groupby
from odoo import api, fields, models, _


class InheritAccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
         
    
    #Program Terbilang
    def Terbilang(self,bil):
        angka=("","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh",
                "Delapan","Sembilan","Sepuluh","Sebelas")
        Hasil =" "
        n = int(bil)
        if n >= 0 and n <= 11:
            Hasil = Hasil + angka[n]
        elif n < 20:
            Hasil = self.Terbilang(n % 10) + " belas"
        elif n < 100:
            Hasil = self.Terbilang(n / 10) + " Puluh" + self.Terbilang(n % 10)
        elif n < 200:
            Hasil = "Seratus" + self.Terbilang(n - 100)
        elif n < 1000:
            Hasil = self.Terbilang(n / 100) + " Ratus" + self.Terbilang(n %100)
        elif n < 2000:
            Hasil = "Seribu" + self.Terbilang(n-1000)
        elif n < 1000000:
            Hasil = self.Terbilang(n / 1000) + " Ribu" + self.Terbilang(n % 1000)
        elif n < 1000000000:
            Hasil = self.Terbilang(n/1000000) + " Juta" + self.Terbilang(n % 1000000)
        else:
            Hasil = self.Terbilang(n / 1000000000) + " Milyar" + self.Terbilang(n % 1000000000)
        return Hasil
    
    @api.depends('amount_total')
    def compute_terbilang(self): 
        for rec in self:
            print("====== test ======",rec.amount_total)
            blng= self.Terbilang(rec.amount_total)
            rp=" Rupiah"
            
            rec.terbilang = str(blng+rp)
    
    terbilang = fields.Char(string='Terbilang', compute='compute_terbilang', store=True)  
    
