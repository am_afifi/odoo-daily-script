@api.model
    def create(self,vals):
        project_ids = []
        stage_obj = self.env['project.task.type']
        result = super(Project, self).create(vals)
        for resource in result:
            for source in resource.stage_ids:
                if source:
                    stage_id = stage_obj.search([('id', '=',source.name.id)])
                    project_ids.append(result.id)
                    stage_id.update({'project_ids': [( 6, 0, project_ids)]})
        return result

or

@api.model
    def create(self, vals):
        stage_obj = self.env['project.task.type']
        result = super(Project, self).create(vals)
        for resource in result.stage_ids:
            stage_id = stage_obj.search([('id', '=',resource.name.id)])
            if stage_id:
                stage_id.write({'project_ids': [( 4, result.id)]})
        return result

#stage_id_s.write({'project_ids': [( 3, self.id)]}) (unlink)
#stage_id.write({'project_ids': [( 4, result.id)]}) (create new)

# for insert new record to m2m field
@api.onchange('price_subtotal')
    def _onchange_price_subtotal(self):
        for x in self:
            if x.price_subtotal >= x.product_id.product_tmpl_id.pph_sale_min:
                x.tax_id = [(4, x.product_id.product_tmpl_id.pph_sale_id.id)]


#For Many2many
For a many2many field, a list of tuples is expected. Here is the list of tuple that are accepted, with the corresponding semantics:
(0, 0, { values }) link to a new record that needs to be created with the given values dictionary
(1, ID, { values }) update the linked record with id = ID (write values on it)
(2, ID) remove and delete the linked record with id = ID (calls unlink on ID, that will delete the object completely, and the link to it as well)
(3, ID) cut the link to the linked record with id = ID (delete the relationship between the two objects but does not delete the target object itself)
(4, ID) link to existing record with id = ID (adds a relationship)
(5) unlink all (like using (3,ID) for all linked records)
(6, 0, [IDs]) replace the list of linked IDs (like using (5) then (4,ID) for each ID in the list of IDs)

Example: [(6, 0, [8, 5, 6, 4])] sets the many2many to ids [8, 5, 6, 4]


And One2many :
(0, 0, { values }) link to a new record that needs to be created with the given values dictionary
(1, ID, { values }) update the linked record with id = ID (write values on it)
(2, ID) remove and delete the linked record with id = ID (calls unlink on ID, that will delete the object completely, and the link to it as well)

Example: [(0, 0, {‘field_name’:field_value_record1, …}), (0, 0, {‘field_name’:field_value_record2, …})]
