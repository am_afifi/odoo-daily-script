#1). I create some js script (tree_menu/static/src/js/tree_view_button.js) with click listener for my button :

odoo.define('tree_menu.tree_view_button', function (require){
"use strict";

var core = require('web.core');
var ListView = require('web.ListView');
var QWeb = core.qweb;

ListView.include({       

        render_buttons: function($node) {
                var self = this;
                this._super($node);
                    this.$buttons.find('.o_list_tender_button_create').click(this.proxy('tree_view_action'));
        },

        tree_view_action: function () {           

        this.do_action({
                type: "ir.actions.act_window",
                name: "product",
                res_model: "product.template",
                views: [[false,'form']],
                target: 'current',
                view_type : 'form',
                view_mode : 'form',
                flags: {'form': {'action_buttons': true, 'options': {'mode': 'edit'}}}
        });
        return { 'type': 'ir.actions.client','tag': 'reload', } } 

});

});


#2). After that, create tree_menu/static/src/xml/tree_view_button.xml with the template, which replaces “Create” button if I use project.project model

<?xml version="1.0" encoding="UTF-8"?>
      <template id="template" xml:space="preserve">
          <t t-extend="ListView.buttons">
            <t t-jquery="button.o_list_button_add" t-operation="replace">
                  <button t-if="widget.model == 'purchase.order'" class="btn btn-primary btn-sm o_list_tender_button_create" type="button"&gt;Create Tender</button>
                  <button t-if="widget.model != 'purchase.order'" class="btn btn-primary btn-sm o_list_button_add" type="button"&gt;Create</button>
            </t>
          </t>
      </template>


#3). After that, add js script in web.asset_backend (Create file tree_menu/views/tree_view_asset.xml)

<?xml version="1.0" encoding="utf-8"?>
<odoo>
      <data>
            <template id="assets_backend" name="tree view menu" inherit_id="web.assets_backend">
                  <xpath expr="." position="inside">
                        <script type="text/javascript" src="/tree_menu/static/src/js/tree_view_button.js"></script>
                  </xpath>
            </template>
      </data>
</odoo>


#4). And finally, add in __manifest__.py section ‘qweb’ for ‘qweb’: [‘static/src/xml/tree_view_button.xml’], and place file views/project.xml in ‘data’ section.

{
    'name': 'odoo10 Tree View JS Menu',
    'version': '1.0',
    'category': 'General',
    'summary': 'odoo10 Tree View JS Menu',
    'description': """ odoo10 Tree View JS Menu """,
    'author': 'Ananthu',
    'website': 'http://www.codersfort.com',
    'depends': ['base','purchase','web'],
    'data':[
            'views/tree_view_asset.xml',
            ],
    'qweb': ['static/src/xml/tree_view_button.xml'],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
