import pytz
from pytz import timezone

    def _get_date_formats(self, date):
        lang_code = self.env.user.lang or "en_US"
        lang = self.env["res.lang"]._lang_get(lang_code)
        date_format = lang.date_format
        time_format = lang.time_format
        fmt = date_format + " " + time_format
        utc_tz = pytz.utc.localize(date, is_dst=False)
        user_tz = self.env.user.tz
        user_tz = user_tz and timezone(self.env.user.tz) or timezone("GMT")
        final = utc_tz.astimezone(user_tz)

        return final.strftime(fmt)

# simple mode
format = pytz.utc.localize(date).astimezone(timezone('Asia/Jakarta'))
