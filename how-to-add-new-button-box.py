class Employee(models.Model):
    _inherit = "hr.employee"
 
 
    @api.multi
    def view_onboard(self):
        onboard_ids = []
        action = self.env.ref('feag_hr.open_view_onboarding_action').read()[0]
 
        onboard = self.env['employee.onboard'].search([('name', '=', self.id)])
        for data in onboard:
            onboard_ids.append(data.id)
 
        action['domain'] = [('id', 'in', onboard_ids)]
        return action

# XML
<record id="cf_hr_onboard_view_employee_form" model="ir.ui.view">
    <field name="name">hr.employee.form</field>
    <field name="model">hr.employee</field>
    <field name="inherit_id" ref="hr.view_employee_form"/>
    <field name="arch" type="xml">
        <xpath expr="//div[@name='button_box']" position="inside">
            <button name="view_onboard" type="object" string="Onboarding" groups="hr.group_hr_user"
                                class="oe_stat_button" icon="fa-file-text">
            </button>
        </xpath>
    </field>
</record>
