@api.multi
def write(self, vals):
    self.env['ir.rule'].clear_cache()
    return models.Model.write(self, vals)
