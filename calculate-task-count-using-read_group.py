#Projects Domain
if project_id == 'all_projects':
    total_tasks_domain = ['|', ('stage_id.fold', '=', False), ('stage_id', '=', False)]
else:
    total_tasks_domain = [('project_id', 'in', [int(project_id)]), '|', ('stage_id.fold', '=', False), ('stage_id', '=', False)]
task_data = self.env['project.task'].read_group(
    domain=total_tasks_domain, 
    fields=['project_id'], 
    groupby='project_id')

mapped_data = dict([(data['project_id'], data['project_id_count']) for data in task_data])
for data in mapped_data.values():
    tasks_count += data
