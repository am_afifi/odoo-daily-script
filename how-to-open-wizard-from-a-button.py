#Menu Action
<xpath expr="//field[@name='backorder_id']" position="after">
<button name="change_location" attrs="{'invisible': ['|',('state','=','done'),('picking_type_code', 'in', ('internal', 'outgoing'))]}" type="object" string="Change Destination Location"/>                    
 </xpath>

@api.multi
    def change_location(self):
        
        view = self.env.ref('fair_construction.view_change_dest_location')
        wiz = self.env['change.dest.location'].create({'location_dest_id': self.location_dest_id.id,
                                                       'picking_id' : self.id})
        # TDE FIXME: a return in a loop, what a good idea. Really.
        return {
            'name': _('Change Destination Location'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'change.dest.location',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
            'context': self.env.context,
        }


#Wizard Action
from odoo import models, fields, api, _


class ChangeDestLocation(models.TransientModel):
    _name = 'change.dest.location'

    location_dest_id = fields.Many2one(
        'stock.location', "Destination Location Zone",
        required=True)
    
    picking_id = fields.Many2one('stock.picking', "Picking",
        required=True, ondelete='cascade')
    

    
    @api.multi
    def change_location(self):
        
        if self.location_dest_id and self.picking_id and self.picking_id.pack_operation_product_ids:
            #update destination in operation
            picking_obj = self.env['stock.picking']            
            picking_id = picking_obj.sudo().search([('id','=',self.picking_id.id)])
            if picking_id:
                picking_id.sudo().write({
                    'location_dest_id' : self.location_dest_id.id,
                    'is_issued_to_central_site': True if self.location_dest_id.is_central_stock else False,
                })
            
            #update destination in operation pack
            for operations in self.picking_id.pack_operation_product_ids:
                operations.location_dest_id = self.location_dest_id.id
                
            #update destination in moves
            for move in self.picking_id.move_lines:
                move.location_dest_id = self.location_dest_id.id
#XML Wizard
<odoo>
    <data>
        <record id="view_change_dest_location" model="ir.ui.view">
        <field name="name">Change Destination Location</field>
        <field name="model">change.dest.location</field>
        <field name="arch" type="xml">
        <form string="Choose New Location">
                <p class="oe_gray">
                        Select new location
                </p>
                <group>
                        <group>
                                <field name="location_dest_id"/>
                        </group>
                </group>
                <footer>
                        <button name="change_location" 
                                string="Apply" 
                                type="object" 
                                class="btn-primary"/>
                        <button string="Cancel" class="btn-default" special="cancel" />
                </footer>
        </form>
        </field>
        </record>

        <record id="action_change_dest_location" model="ir.actions.act_window">
	        <field name="name">Change Destination Location</field>
	        <field name="res_model">change.dest.location</field>
	        <field name="view_type">form</field>
	        <field name="view_mode">form</field>
	        <field name="view_id" ref="view_change_dest_location"/>
	        <field name="target">new</field>
        </record>
        
    </data>
</odoo>
