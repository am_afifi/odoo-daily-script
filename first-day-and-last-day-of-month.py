import calendar
import datetime
 
@api.multi
def _get_nextcall_monthly_leave(self):
    date = datetime.date.today()
    start_date = datetime.datetime(date.year, date.month, 1)
    end_date = datetime.datetime(date.year, date.month, calendar.mdays[date.month])
    return end_date
