emp_asses_ques_rev_ids = fields.One2many('employee.assessment.questions.review', 'performance_id', 'Employee Assessment questions Review')

@api.onchange('period_id')
def onchange_period_id(self): 
  for record in self.emp_asses_ques_rev_ids:
      self.write({'emp_asses_ques_rev_ids': [(2,record.id)]})
