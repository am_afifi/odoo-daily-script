    
    @api.multi
    def action_submit(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for peers in self.peer_employee_ids:
            _url = ''+ base_url +'/peer_feedback/'+ str(self.id) +'/'
            button_name = str("Employee Peer Feedback")
            if peers.user_id.partner_id.id:
                subject = 'Employee Peer Feedback Form for %s' % peers.name                        
                body_html = '<p>Dear '+ peers.name +',<br />\
                            We request your feedback for the employee '+ self.employee_id.name +' as part \
                            of the Performance Review. Click on the link below to submit your \
                            comments.</p><br /><p><a href=%s class="btn btn-danger">Employee Peer Feedback</a><br></p>' % _url                
                mail = self.env['mail.mail'].create({
                    'subject': subject,
                    'body_html': body_html,
                    'notification': True,
                    'state': 'outgoing',
                    'recipient_ids': [(4, peers.user_id.partner_id.id)]
                })
        self.state = 'submitted'
