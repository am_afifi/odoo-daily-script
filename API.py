# WARNING: before calling /api/auth, call /web?db=*** otherwise web service is not found.

    def action_send_to_rbac(self):
        if not self.id_rbhc:
            if not self.kode_projek:
                raise Warning('Kode proyek masih kosong! mohon masukan kode proyek')
            if not self.tgl_mulai_projek:
                raise Warning('Tanggal proyek masih kosong! mohon masukan tanggal proyek')
            if not self.branch_id:
                raise Warning('Divisi/Branch masih kosong! mohon masukan nama Divisi/Branch proyek')
            access_token = self.env.user.jwtToken
            url = self.env['ir.config_parameter'].sudo().get_param('config_url_rbhc')
            headers = {
                'Authorization': 'Bearer %s' % access_token,
                'content-type': 'application/json'
            }
            dt1 = datetime.strftime(self.tgl_mulai_projek, "%Y-%m-%d")
            dt1_fmt = datetime.strptime(dt1, "%Y-%m-%d")
            timestamp1 = datetime.timestamp(dt1_fmt)
            to_str1 = str(timestamp1)
            data = {
                "parentId": self.branch_id.parent_id_rbhc,
                "pProyekId": "",
                "tJobUnitId": "",
                "erpUnitId": self.branch_id.id,
                "erpProyekId": self.id,
                "kode": self.kode_projek,
                "nama": self.name,
                "validDari": to_str1[:10]+'000',
                "validSampai": ""
            }
            res = requests.post(url, headers=headers, json=data)
            jsonResult = res.content.decode('utf-8')
            result = json.loads(jsonResult)
            self.id_rbhc = result['rowData']['tunitId']

    def send_hak_akses(self):
        self.action_send_to_rbac()
        access_token = self.env.user.jwtToken
        get_url = self.env['ir.config_parameter'].sudo().get_param('config_url_hak_akses_rbac')
        index_url = get_url.find('unit')
        get_code_perusahaan = self.env['ir.config_parameter'].sudo().get_param('perusahaan')
        get_code = ''
        if self.branch_id.name == 'Divisi Operasi 1':
            get_code = self.env['ir.config_parameter'].sudo().get_param('divisi 1')
        elif self.branch_id.name == 'Divisi Operasi 2':
            get_code = self.env['ir.config_parameter'].sudo().get_param('divisi 2')
        elif self.branch_id.name == 'Divisi Operasi 3':
            get_code = self.env['ir.config_parameter'].sudo().get_param('divisi 3')

        url_div = '%s%s%s%s' % (get_url[:index_url],get_code,'/',get_url[index_url:])
        url_perusahaan = '%s%s%s%s' % (get_url[:index_url],get_code_perusahaan,'/',get_url[index_url:])
        headers = {
            'Authorization': 'Bearer %s' % access_token,
            'content-type': 'application/json'
        }
        dt1 = datetime.strftime(fields.Date.today(), "%Y-%m-%d")
        dt1_fmt = datetime.strptime(dt1, "%Y-%m-%d")
        timestamp1 = datetime.timestamp(dt1_fmt)
        to_str1 = str(timestamp1)
        data = {
            'createBy': 7365,
            'tDataUnit': [
                {
                    'tunitId': self.id_rbhc,
                    'validDari': to_str1[:10]+'000',
                    'validSampai': None
                }
            ]
        }
        res_div = requests.post(url_div, headers=headers, data=json.dumps(data))
        res_per = requests.post(url_perusahaan, headers=headers, data=json.dumps(data))
