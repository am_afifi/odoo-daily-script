milestone =[]
stages = []

iflen(stages) >len(set(stages)):
   raise ValidationError('Same stages can not be added more than once')
 
iflen(milestone) >len(set(milestone)):
   raise ValidationError('Same milestones can not be added more than once')
